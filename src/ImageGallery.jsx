import UploadSection from "./UploadSection";
import GallerySection from "./GallerySection";
import { useState, useRef } from "react";
import base64StringtoFile from "./base64StringtoFile";
import { CiWarning } from "react-icons/ci";

import "./index.css";

function ImageGallery() {
  //to store url of the uploaded file
  const [uploadedImageUrl, setUploadedImageUrl] = useState("");

  //to store details of the uploaded file object
  const [uploadedImage, setUploadedImage] = useState("");

  //reference for file input element
  let uploadInputRef = useRef();

  //reference for canvas where the preview is shown
  let previewRef = useRef();

  function handleImageUploadSubmit(event) {
    event.preventDefault();

    let value = URL.createObjectURL(event.target.filename.files[0]);

    // to run without api enable below(setUploadedImageUrl) and vice versa
    setUploadedImageUrl(value);

    setUploadedImage(event.target.filename.files[0]);
    uploadInputRef.current.value = "";

    ///////////////////// API Configs //////////////////////

    // to use api
    // const API_ROUTE = "http://localhost:5000/api/urls";
    // const REQUEST_BODY = JSON.stringify({
    //   url: value
    // });

    // fetch(API_ROUTE, {
    //   method: "POST",
    //   headers: { "content-type": "application/json" },
    //   body: REQUEST_BODY
    // })
    //   .then(res => res.json())
    //   .then(data => {
    //     console.log(data);
    //     setUploadedImageUrl(data.url);
    //   })
    //   .catch(err => console.log(err));
  }

  function handleSaveButton() {
    const canvas = previewRef.current;

    // Converting to base64
    const base64Image = canvas.toDataURL("image/jpeg");

    const myNewCroppedFile = base64StringtoFile(
      base64Image,
      // "newCroppedImage.png"
      uploadedImage.name
    );
    // console.log(myNewCroppedFile);

    //replacing original image with the changed image
    let value = URL.createObjectURL(myNewCroppedFile);

    // to run without api enable below(setUploadedImageUrl) and vice versa
    setUploadedImageUrl(value);

    setUploadedImage(myNewCroppedFile);

    ///////////////////// API Configs //////////////////////

    // to use api
    // const API_ROUTE = "http://localhost:5000/api/urls/update";
    // const REQUEST_BODY = JSON.stringify({
    //   url: value
    // });

    // fetch(API_ROUTE, {
    //   method: "PUT",
    //   headers: { "content-type": "application/json" },
    //   body: REQUEST_BODY
    // })
    //   .then(res => res.json())
    //   .then(data => {
    //     console.log(data);
    //     setUploadedImageUrl(data.url);
    //   })
    //   .catch(err => console.log(err));
  }

  return (
    <div className="container mt-3">
      <center>
        <div className="ig-warning-msg">
          <h6>
            <CiWarning style={{ height: "24px", width: "24px" }} />
            <i>
              We're doing some website maintenance. Hence the website may not
              work as expected. Sorry for the inconvenience occured.
            </i>
          </h6>
        </div>
      </center>
      <div className="m-3">
        <h1>Image Gallery</h1>
      </div>

      <div className="d-flex">
        <UploadSection
          handleImageUploadSubmit={handleImageUploadSubmit}
          uploadInputRef={uploadInputRef}
        />
        <GallerySection
          uploadedImageUrl={uploadedImageUrl}
          uploadedImage={uploadedImage}
          handleSaveButton={handleSaveButton}
          previewRef={previewRef}
        />
      </div>
    </div>
  );
}

export default ImageGallery;
