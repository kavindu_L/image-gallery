import ModalBody from "./ModalBody";

function PopupModal({
  uploadedImageUrl,
  uploadedImage,
  handleSaveButton,
  previewRef
}) {
  return (
    <>
      <div
        className="modal fade modal-xl"
        id="staticBackdrop"
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        tabIndex="-1"
        aria-labelledby="staticBackdropLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            {/* modal head */}
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="staticBackdropLabel">
                {uploadedImage.name}
              </h1>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>

            {/* modal body */}
            <div
              className="modal-body"
              style={{ width: "100%", height: "600px" }}
            >
              <ModalBody
                uploadedImageUrl={uploadedImageUrl}
                previewRef={previewRef}
              />
            </div>

            {/* modal foot */}
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-primary"
                onClick={handleSaveButton}
              >
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default PopupModal;
