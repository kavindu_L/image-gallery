import { useState } from "react";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";

function CropImage({ uploadedImageUrl, rotate, previewRef }) {
  const [crop, setCrop] = useState();
  const [imageElement, setImageElemnt] = useState();

  function handleOnCropChange(crop, percentCrop) {
    setCrop(crop);
  }

  function handleOnImageLoaded(event) {
    setImageElemnt(event.target);
  }

  function handleOnComplete(crop, percentCrop) {
    const canvas = previewRef.current;
    const scaleX = imageElement.naturalWidth / imageElement.width;
    const scaleY = imageElement.naturalHeight / imageElement.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext("2d");

    const croppedImage = new Image();
    croppedImage.src = uploadedImageUrl;
    croppedImage.onload = function() {
      ctx.drawImage(
        croppedImage,
        crop.x * scaleX,
        crop.y * scaleY,
        crop.width * scaleX,
        crop.height * scaleY,
        0,
        0,
        crop.width,
        crop.height
      );
    };
  }

  return (
    <div className="d-flex flex-column align-items-center h-100">
      <div>
        <ReactCrop
          src={uploadedImageUrl}
          crop={crop}
          onChange={handleOnCropChange}
          onComplete={handleOnComplete}
        >
          <img
            src={uploadedImageUrl}
            alt=""
            width="400px"
            height="300px"
            style={{ transform: `rotate(${rotate}deg)` }}
            onLoad={handleOnImageLoaded}
          />
        </ReactCrop>
      </div>

      <div className="mt-5">
        <canvas
          ref={previewRef}
          style={{ transform: `rotate(${rotate}deg)` }}
        ></canvas>
      </div>
    </div>
  );
}

export default CropImage;
