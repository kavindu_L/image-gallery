import React from "react";
import ReactDOM from "react-dom/client";
import ImageGallery from "./ImageGallery";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <ImageGallery />
  </React.StrictMode>
);
