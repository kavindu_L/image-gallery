function UploadSection({ handleImageUploadSubmit, uploadInputRef }) {
  return (
    <div
      className="me-3 rounded bg-light p-3 align-self-start"
      style={{ width: "400px" }}
    >
      {/* <div>
        <form onSubmit={handleImageUploadSubmit}>
          <input class="form-control" type="file" id="myFile" name="filename" />
          <input type="submit" />
        </form>
      </div> */}
      <form onSubmit={handleImageUploadSubmit}>
        <div className="input-group input-group-sm mb-3">
          <input
            ref={uploadInputRef}
            className="form-control"
            type="file"
            name="filename"
          />
          <button
            className="btn btn-outline-secondary"
            type="submit"
          >
            upload
          </button>
        </div>
      </form>
    </div>
  );
}

export default UploadSection;
