import PopupModal from "./PopupModal";

function GallerySection({
  uploadedImageUrl,
  uploadedImage,
  handleSaveButton,
  previewRef
}) {
  return (
    <div>
      {uploadedImageUrl.length > 0 && (
        <div className="rounded bg-light p-3" style={{ width: "600px" }}>
          <center>
            <img
              src={uploadedImageUrl}
              alt=""
              width="300px"
              height="200px"
              data-bs-toggle="modal"
              data-bs-target="#staticBackdrop"
            />

            <PopupModal
              uploadedImageUrl={uploadedImageUrl}
              uploadedImage={uploadedImage}
              handleSaveButton={handleSaveButton}
              previewRef={previewRef}
            />

            <h6 className="mt-2">
              <i>{uploadedImage.name}</i>
            </h6>
          </center>
        </div>
      )}
    </div>
  );
}

export default GallerySection;
