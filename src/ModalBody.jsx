import { useState } from "react";
import CropImage from "./CropImage";

function ModalBody({ uploadedImageUrl, previewRef }) {
  const [rotate, setRotate] = useState(0);

  function handleRotateClockwise() {
    setRotate(c => c + 15);
  }

  function handleRotateAntiClockwise() {
    setRotate(c => c - 15);
  }

  return (
    <div className="" style={{ height: "400px" }}>
      <div className="btn-group btn-group-sm mt-1 mb-3">
        <button
          className="btn btn-outline-primary"
          onClick={handleRotateAntiClockwise}
        >
          rotate anti-clockwise
        </button>
        <button
          className="btn btn-outline-primary"
          onClick={handleRotateClockwise}
        >
          rotate clockwise
        </button>
      </div>

      <div>
        <CropImage
          uploadedImageUrl={uploadedImageUrl}
          rotate={rotate}
          previewRef={previewRef}
        />
      </div>
    </div>
  );
}

export default ModalBody;
